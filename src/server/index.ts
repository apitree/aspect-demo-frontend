import * as express from 'express';
import * as nextjs from 'next';
import * as bodyParser from 'body-parser';

const dev: boolean = process.env.NODE_ENV !== 'production';

// inicializace next.js aplikace
const app = nextjs({dev, dir: './dist/client'});
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express();
    server.enable('trust proxy');

    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({extended: false}));

    /**
     * Vsechny routy, smerujici na domenu jsou obslouzeny Next.js
     * Diky explicitni konfiguraci je mozne definovat vlastni routy, ktere budou presmerovavat dane pozadavky na dane stranky
     */
    server.get('*', (req, res) => handle(req, res));

    const PORT = process.env.PORT || 8080;
    server.listen(PORT, (err: Error) => {
        if (err) {
            throw err;
        }
        // tslint:disable-next-line
        console.log(`Server is ready on PORT=${PORT}`);
    });
});
