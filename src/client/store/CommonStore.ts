import {AsyncData} from '../AsyncData';
import {UserModel} from '../model';

export interface CommonStore {
    readonly users: AsyncData<UserModel[]>;
}
