import {CommonStore} from './CommonStore';
import {FormStateMap} from 'redux-form';

export interface Store {
    readonly common: CommonStore;
    readonly form: FormStateMap;
}
