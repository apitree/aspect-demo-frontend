import {bindActionCreators} from 'redux';
import {Dispatch} from 'react-redux';
import {Store} from '../store';
import {UserModel} from '../model';

const PREFIX = 'COMMON_';

const CommonActionType = {
    FETCHING_USERS: `${PREFIX}FETCHING_USERS`,
    FETCHED_USERS: `${PREFIX}FETCHED_USERS`,
    ERROR_FETCHED_USERS: `${PREFIX}ERROR_FETCHED_USERS`,
};

interface CommonAction {
    readonly fetchUsers: () => (dispatch: Dispatch<Store>) => void;
}

const CommonActionImpl: CommonAction = {

    fetchUsers: () => (dispatch: Dispatch<Store>) => {
        dispatch({type: CommonActionType.FETCHING_USERS});
        // fake server call
        setTimeout(() => {
            const users: UserModel[] = [
                {id: '1', firstName: 'Ales', lastName: 'Dostal'},
                {id: '2', firstName: 'Josef', lastName: 'Vomacka'},
                {id: '3', firstName: 'Martin', lastName: 'Novak'},
                {id: '4', firstName: 'Jana', lastName: 'Borovickova'},
                {id: '5', firstName: 'Aspect', lastName: 'Aspectovic'},
            ];
            dispatch({type: CommonActionType.FETCHED_USERS, payload: users});

            // dispatch({type: CommonActionType.FETCHED_USERS, payload: err.message});

        }, 200);
    },

};

const CommonActionCreator = (dispatch: Dispatch<Store>) => bindActionCreators({...CommonActionImpl}, dispatch);

export {CommonActionType, CommonAction, CommonActionCreator};
