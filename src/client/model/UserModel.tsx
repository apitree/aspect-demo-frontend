export interface UserModel {
    readonly id: string;
    readonly firstName: string;
    readonly lastName: string;
}
