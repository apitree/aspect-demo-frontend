import * as React from 'react';
import {MuiThemeProvider, withStyles, Theme} from 'material-ui/styles';
import {getContext} from '../styles/context';
// tslint:disable-next-line
const wrapDisplayName = require('recompose/wrapDisplayName').default;

// Apply some reset
const styles = (theme: Theme) => ({
    '@global': {
        html: {
            background: theme.palette.background.default,
            WebkitFontSmoothing: 'antialiased', // Antialiasing.
            MozOsxFontSmoothing: 'grayscale', // Antialiasing.
        },
        body: {
            margin: 0,
        },
    },
});

const AppWrapper = withStyles(styles)((props: any) => props.children);

export const withMaterialUI = (BaseComponent: any) => {

    class WithRoot extends React.Component {

        private styleContext: any;

        static async getInitialProps(ctx: any) {
            if (BaseComponent.getInitialProps) {
                return BaseComponent.getInitialProps(ctx);
            }
            return {};
        }

        componentWillMount() {
            this.styleContext = getContext();
        }

        componentDidMount() {
            // Remove the server-side injected CSS.
            const jssStyles = document.querySelector('#jss-server-side');
            if (jssStyles && jssStyles.parentNode) {
                jssStyles.parentNode.removeChild(jssStyles);
            }
        }

        render() {
            const {theme, sheetsManager} = this.styleContext;
            return (
                <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
                    <AppWrapper>
                        <BaseComponent {...this.props} />
                    </AppWrapper>
                </MuiThemeProvider>
            );
        }
    }

    if (process.env.NODE_ENV !== 'production') {
        (WithRoot as any).displayName = wrapDisplayName(BaseComponent, 'withRoot');
    }

    return WithRoot;
};
