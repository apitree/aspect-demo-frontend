import * as React from 'react';
import * as withRedux from 'next-redux-wrapper';
import {withMaterialUI} from './with/withMaterialUI';
import Router from 'next/router';
import * as NProgress from 'nprogress';

// NProgress.configure({parent: '#loadingContent'});

Router.onRouteChangeStart = (url) => {
    console.log('Start waiting');
    NProgress.start();
};
Router.onRouteChangeComplete = () => {
    console.log('End waiting');
    NProgress.done();
};
Router.onRouteChangeError = () => NProgress.done();

/**
 * Hlavni boot aplikace, ktera pridava Redux, Fela.
 *
 * @param createStore hlavni Redux store
 * @param mapStateToProps mapovani redux store do komponenty
 * @param mapDispatchToProps mapovani dispatch akci do komponenty
 */
export const app = (createStore: any, mapStateToProps: any, mapDispatchToProps: any) => (ComposedComponent: any) => {
    return withRedux(
        createStore,
        mapStateToProps,
        mapDispatchToProps,
    )(withMaterialUI(ComposedComponent));
};
