import {createMuiTheme} from 'material-ui/styles';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';
// tslint:disable-next-line
const {create, SheetsRegistry} = require('jss');
// tslint:disable-next-line
const preset = require('jss-preset-default').default;

declare const process: any;
declare const global: any;

// Konfigurace JSS
const jss = create(preset());
jss.options.createGenerateClassName = createGenerateClassName;

const createContext = (type: 'dark' | 'light') => ({
    jss,
    theme: createMuiTheme(),
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
});

export const getContext = (type: 'dark' | 'light' = 'light') => {
    // Make sure to create a new store for every server-side request so that data
    // isn't shared between connections (which would be bad)
    if (!process.browser) {
        return createContext(type);
    }

    // Reuse context on the client-side
    if (!global.__INIT_MATERIAL_UI__) {
        global.__INIT_MATERIAL_UI__ = createContext(type);
    }
    return global.__INIT_MATERIAL_UI__;
};
