import * as React from 'react';
import Document, {Head, Main, NextScript} from 'next/document';
import {getContext} from '../styles/context';
// tslint:disable-next-line
const JssProvider = require('react-jss/lib/JssProvider').default;

class MainDocument extends Document {

    static async getInitialProps(ctx: any) {
        const context = getContext();
        const page = ctx.renderPage((Component: any) => (props: any) => (
            <JssProvider registry={context.sheetsRegistry} jss={context.jss}>
                <Component {...props} />
            </JssProvider>
        ));

        return {
            ...page,
            stylesContext: context,
            styles: (<style id="jss-server-side" dangerouslySetInnerHTML={{__html: context.sheetsRegistry.toString()}}/>),
        };
    }

    render() {
        const {stylesContext} = this.props;
        return (
            <html lang="en" dir="ltr">
            <Head>
                <title>AspectWorks - Demo</title>
                <meta charSet="utf-8"/>
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="robots" content="noindex"/>
                <meta name="viewport" content="user-scalable=0, initial-scale=1, minimum-scale=1, width=device-width, height=device-height"/>
                <meta name="keywords" content=""/>
                <meta name="description" content=""/>
                <meta name="author" content="ApiTree s.r.o."/>
                <link rel="shortcut icon" type="image/x-icon" href="/static/images/favicon.png"/>
                <link rel="icon" type="image/x-icon" href="/static/images/favicon.png"/>
                <meta name="theme-color" content={stylesContext.theme.palette.primary[500]}/>
                <link rel="stylesheet" type="text/css" href="/static/styles/nprogress.css"/>
            </Head>
            <body>
            <Main/>
            <NextScript/>
            </body>
            </html>
        );
    }
}

export default MainDocument;
