import * as React from 'react';
import {Dispatch} from 'redux';
import {Button, Grid, Typography} from 'material-ui';
import {app} from '../app';
import {createStore} from '../createStore';
import {CommonStore, Store} from '../store';
import {CommonAction, CommonActionCreator} from '../actions';
import Link from 'next/link';
import {HelpUtil} from '../utils/HelpUtil';

interface OwnProps {
}

interface ConnectedState {
    readonly common: CommonStore;
}

interface ConnectedDispatch extends CommonAction {
}

class Index extends React.Component<ConnectedState & ConnectedDispatch & OwnProps> {

    static async getInitialProps(ctx: any) {
        await HelpUtil.wait(3000);
        return {};
    }

    render() {
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Typography type="title">Forever page...</Typography>
                    <Link href="/">
                        <Button raised color="primary">Go to Index</Button>
                    </Link>
                </Grid>
            </Grid>
        );
    }
}

export default app(
    createStore,
    ({common}: Store): ConnectedState => ({common}),
    (dispatch: Dispatch<Store>): ConnectedDispatch => CommonActionCreator(dispatch),
)(Index);
