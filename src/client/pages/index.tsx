import * as React from 'react';
import {Dispatch} from 'redux';
import {Button, Grid, Typography} from 'material-ui';
import {app} from '../app';
import {createStore} from '../createStore';
import {CommonStore, Store} from '../store';
import {CommonAction, CommonActionCreator} from '../actions';
import Link from 'next/link';

interface OwnProps {
}

interface ConnectedState {
    readonly common: CommonStore;
}

interface ConnectedDispatch extends CommonAction {
}

class Index extends React.Component<ConnectedState & ConnectedDispatch & OwnProps> {

    render() {
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Typography type="title">Hello from Next.js</Typography>
                    <Link href="/forever">
                        <Button raised color="primary">Go to Forever</Button>
                    </Link>
                </Grid>
            </Grid>
        );
    }
}

export default app(
    createStore,
    ({common}: Store): ConnectedState => ({common}),
    (dispatch: Dispatch<Store>): ConnectedDispatch => CommonActionCreator(dispatch),
)(Index);
