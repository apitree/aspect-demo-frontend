import {Action, handleActions} from 'redux-actions';
import {CommonActionType as ActionType} from '../actions';
import {CommonStore as Store} from '../store';
import {UserModel} from '../model';

const initialState = {
    users: {isFetching: false, isFetched: false, data: []},
} as Store;

export const CommonReducer = handleActions<Store, any>({

    [ActionType.FETCHING_USERS]: (state: Store): Store => ({
        ...state, users: {...state.users, isFetching: true, isFetched: false},
    }),

    [ActionType.FETCHED_USERS]: (state: Store, {payload}: Action<UserModel[]>): Store => ({
        ...state,
        users: {...state.users, isFetching: false, isFetched: true, data: payload, lastFetched: new Date(), errorMessage: null},
    }),

    [ActionType.ERROR_FETCHED_USERS]: (state: Store, {payload}: Action<string>): Store => {
        return {...state, users: {...state.users, isFetching: false, isFetched: false, errorMessage: payload}};
    },

}, initialState);
