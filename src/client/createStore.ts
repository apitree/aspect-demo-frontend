import {applyMiddleware, compose, createStore as reduxCreateStore, StoreCreator} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {Store} from './store';
import {rootReducer} from './rootReducer';

declare const window: any;

/**
 * Kontrola, ze existuje Redux DEV tools v prohlizeci (Chrome).
 * Pro Chrome existuje plugin s nazev Redux Dev Tools.
 *
 * @param {boolean} isServer true = kod je vykonavan na severu, false = na klientske strane (browser)
 */
const isEnableReduxDevTools = (isServer: boolean) => {
    return !isServer && window.__REDUX_DEVTOOLS_EXTENSION__ && process.env.NODE_ENV === 'development';
};

/**
 * Vraci store objekt. V pripade dev rezimu a nainstalovaneho Redux Dev Tools vraci store objekt s wrapperem.
 *
 * @param {StoreCreator} store Create store objekt
 * @param {boolean} isServer true = kod je vykonavan na severu, false = na klientske strane (browser)
 */
const reduxTools = (store: StoreCreator, isServer: boolean) => {
    return isEnableReduxDevTools(isServer) ? window.__REDUX_DEVTOOLS_EXTENSION__()(store) : store;
};

/**
 * Vytvari Redux store pro aplikaci. Store obsahuje Thunk middleware pro asynchronni akce.
 * Redux store je compozice z rootReducers
 *
 * @param {Store} initialState vychozi stavi redux store; muze byt prazdny, protoze zakladni inicializaci maji i samotne reducery
 * @param {isServer}: any true = kod je vykonavan na severu, false = na klientske strane (browser)
 * @returns {Store<Store>} Redux store
 */
export const createStore: any = (initialState: Store, {isServer}: any) => {
    if (!isServer && typeof localStorage !== 'undefined' && localStorage !== null) {
        const reduxState = localStorage.getItem('reduxState');
        const persistedState = reduxState ? JSON.parse(reduxState) : {};
        initialState = {...initialState, ...persistedState};
    }
    const store = compose(applyMiddleware(thunkMiddleware))(reduxTools(reduxCreateStore, isServer))(rootReducer, initialState);
    store.subscribe(() => {
        if (!isServer && typeof localStorage !== 'undefined' && localStorage !== null) {
            localStorage.setItem('reduxState', JSON.stringify(store.getState()));
        }
    });
    return store;
};
