import {combineReducers} from 'redux';
import {CommonReducer} from './reducers';
import {reducer as formReducer} from 'redux-form';

/**
 * Root reducer je kompozice ostatnich reduceru z modules.
 * Pokud pridavate novy reducer, nezapomente pridat i jeji typovou definici do souboru ./Store.ts
 *
 * @type {Reducer<any>} root reducer
 */
export const rootReducer = combineReducers({
    common: CommonReducer,
    form: formReducer,
});
