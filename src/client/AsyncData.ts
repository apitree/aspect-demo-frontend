export interface AsyncData<T> {
    readonly data?: T | null;
    readonly isFetching: boolean;
    readonly isFetched: boolean;
    readonly lastFetched?: Date;
    readonly errorMessage?: string | null;
}
