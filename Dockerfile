FROM node:carbon

MAINTAINER Ales Dostal <a.dostal@apitree.cz>

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

# Build application, because Next.js have random HASH and BULD_ID
RUN cd /usr/src/app && npm run build

EXPOSE 8080
CMD ["npm", "start"]
