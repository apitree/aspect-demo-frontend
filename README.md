# AspectWorks Workshop DEMO Projekt
> Projekt slouzici pro demonstrativni ucely

## Pouzite technologie
* Typescript/Javascript
* React
* Redux
* Next.js
* Express
* Material UI
* Docker

## Spusteni projektu
> Skripty v package.json jsou definovany pro UNIX systemy (Linux, MacOS). V pripade Windows je treba vytvorit nove a nebo projekt spoustet v Dockeru

### Development
> Vychozi port je 8080, zmena se provede nastavenim promene PORT

`npm run dev`

Zmena portu

`PORT=xx npm run dev`

V Dockeru

`npm run docker:dev`

### Produkce
> Vychozi port je 8080

`npm run build && npm run start`

Zmena portu

`npm run build && PORT=xxx run start`

V Dockeru

`npm run docker:build && npm run docker:start`

